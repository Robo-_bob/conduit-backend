const winston = require('winston');

const files = new winston.transports.File({ filename: 'events.log' });

const logger = winston.createLogger({
  level: 'info',
  format: winston.format.json(),
  defaultMeta: { service: 'conduit-backend-service' },
  transports: [
    new winston.transports.Console()
  ]
})
.add(files)

module.exports = logger